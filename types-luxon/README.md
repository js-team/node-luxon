# Installation
> `npm install --save @types/luxon`

# Summary
This package contains type definitions for luxon (https://github.com/moment/luxon#readme).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/luxon.

### Additional Details
 * Last updated: Fri, 19 Jan 2024 22:06:57 GMT
 * Dependencies: none

# Credits
These definitions were written by [Carson Full](https://github.com/carsonf), and [Piotr Błażejewicz](https://github.com/peterblazejewicz).
